<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'blog');

Auth::routes();

// web
Route::get('/',                     'Front\FrontController@index')->name('blog');
Route::get('publicacion/{slug}',    'Front\FrontController@post')->name('post');
Route::get('categorias/{slug}',     'Front\FrontController@category')->name('category');
Route::get('etiquetas/{slug}',      'Front\FrontController@tag')->name('tag');


// admin
Route::middleware(['auth', 'admin'])->prefix('admin')->name('admin')->group(function () {

    Route::get('',                       'Backend\BackController@index')->name('');
    
    // CATEGORIES
    Route::get('categories',             'Backend\CategoryController@index')->name('.categories');
    Route::get('categories/create',      'Backend\CategoryController@create')->name('.categories.create');
    Route::post('categories/',           'Backend\CategoryController@store')->name('.categories.store');
    Route::get('categories/{slug}',      'Backend\CategoryController@show')->name('.categories.show');
    Route::get('categories/edit/{id}',   'Backend\CategoryController@edit')->name('.categories.edit');
    Route::post('categories/edit/{id}',  'Backend\CategoryController@update')->name('.categories.update');
    Route::delete('categories/{id}',     'Backend\CategoryController@destroy')->name('.categories.destroy');

    // TAGS    
    Route::get('tags',                   'Backend\TagController@index')->name('.tags');
    Route::get('tags/create',            'Backend\TagController@create')->name('.tags.create');
    Route::post('tags/',                 'Backend\TagController@store')->name('.tags.store');
    Route::get('tags/{slug}',            'Backend\TagController@show')->name('.tags.show');
    Route::get('tags/edit/{id}',         'Backend\TagController@edit')->name('.tags.edit');
    Route::post('tags/edit/{id}',        'Backend\TagController@update')->name('.tags.update');
    Route::delete('tags/{id}',           'Backend\TagController@destroy')->name('.tags.destroy');

    // POSTS    
    Route::get('posts',                   'Backend\PostController@index')->name('.posts');
    Route::get('posts/create',            'Backend\PostController@create')->name('.posts.create');
    Route::post('posts/',                 'Backend\PostController@store')->name('.posts.store');
    Route::get('posts/{slug}',            'Backend\PostController@show')->name('.posts.show');
    Route::get('posts/edit/{id}',         'Backend\PostController@edit')->name('.posts.edit');
    Route::put('posts/edit/{id}',         'Backend\PostController@update')->name('.posts.update');
    Route::delete('posts/{id}',           'Backend\PostController@destroy')->name('.posts.destroy');

    // USERS
    Route::get('users',                   'Backend\UserController@index')->name('.users');
    Route::get('users/create',            'Backend\UserController@create')->name('.users.create');
    Route::post('users/',                 'Backend\UserController@store')->name('.users.store');
    Route::get('users/{slug}',            'Backend\UserController@show')->name('.users.show');
    Route::get('users/edit/{id}',         'Backend\UserController@edit')->name('.users.edit');
    Route::post('users/edit/{id}',        'Backend\UserController@update')->name('.users.update');
    Route::delete('users/{id}',           'Backend\UserController@destroy')->name('.users.destroy');

    // roles
    Route::get('roles',                   'Backend\RoleController@index')->name('.roles');
    Route::get('roles/create',            'Backend\RoleController@create')->name('.roles.create');
    Route::post('roles/',                 'Backend\RoleController@store')->name('.roles.store');
    Route::get('roles/{id}',              'Backend\RoleController@show')->name('.roles.show');
    Route::get('roles/edit/{id}',         'Backend\RoleController@edit')->name('.roles.edit');
    Route::post('roles/edit/{id}',        'Backend\RoleController@update')->name('.roles.update');
    Route::delete('roles/{id}',           'Backend\RoleController@destroy')->name('.roles.destroy');

});


