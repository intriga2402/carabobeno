<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ secure_asset('css/backend/fontawesome-free/css/all.css') }}" rel="stylesheet">
    <link href="{{ secure_asset('css/backend/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ secure_asset('css/backend/dist/css/adminlte.min.css') }}" rel="stylesheet">
    <link href="{{ secure_asset('css/backend/select2/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ secure_asset('css/backend/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ secure_asset('css/backend/icheck-bootstrap/icheck-bootstrap.min.css') }}" rel="stylesheet">
</head>
<body class="@yield('body-class')">
    <div id="app">        
        
        {{-- @if (session('info'))
          <div class="container">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                <div class="alert alert-success">
                  {{ session('info') }}    
                </div>
              </div>
            </div>
          </div>
        @endif

        @if (count($errors))
          <div class="container">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                <div class="alert alert-danger">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>    
                    @endforeach
                  </ul>
                </div>
              </div>
            </div>
          </div>    
        @endif --}}

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @yield('content')
                    
    </div>

    <!-- Scripts -->
    <script src="{{ secure_asset('js/bootstrap/jquery.js') }}"></script>
    <script src="{{ secure_asset('js/backend/bootstrap/js/bootstrap.bundle.min.js') }}" defer></script>
    <script src="{{ secure_asset('js/backend/adminlte.js') }}" defer></script>
    <script src="{{ secure_asset('js/backend/demo.js') }}" defer></script>
    @stack('scripts')
    
</body>
</html>
