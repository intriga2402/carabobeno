@extends('layouts.front')

@section('content')
<!-- Navigation -->
@include('front.sections.navbar')

  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <!-- Blog Entries Column -->
      <div class="col-md-8 offset-md-2">

        <h2 class="my-4">{!! $post->name !!}
        </h2>

        <!-- Blog Post -->
        Categoria 
          <a href="{{ route('category', $post->category->slug) }}">{{ $post->category->name }}</a>
        <div class="card mb-4">
                      
            @if ($post->file)
                <img class="card-img-top img-fluid" src="{{ $post->file }}" alt="Card image cap">
            @endif
            <div class="card-body">
                {{-- <h2 class="card-title">{{ $post->name }}</h2> --}}
                <p class="card-text">{!! $post->body !!}</p>
                <a href="{{ route('blog') }}" class="btn btn-primary">&#8592; Go Back</a>
            </div>

            @foreach ($post->tags as $tag)
                <a href="{{ route('tag', $tag->slug) }}"  class="card-text">
                <p>{{ $tag->name }}</p>
                </a>
            @endforeach

            <div class="card-footer text-muted">
            {{-- Posted on January 1, 2017  --}}
            Posted on
            {{ Carbon\Carbon::parse($post->created_at)->format('l jS \\of F Y') }}
                by
                <b>{!! $post->user->name !!}</b>
            </div>
        </div>  
        

      </div>

      <!-- Sidebar Widgets Column -->
      {{-- @include('front.sections.sidebar') --}}

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  @include('front.sections.footer')
@endsection