@extends('layouts.front')

@section('content')
<!-- Navigation -->
@include('front.sections.navbar')

  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <!-- Blog Entries Column -->
      <div class="col-md-8 offset-md-2">

        <h1 class="my-4">Page Heading
          <small>Secondary Text</small>
        </h1>

        <!-- Blog Post -->
        @foreach ($posts as $post)

        <div class="card mb-4">
            @if ($post->file)
                <img class="card-img-top img-fluid" src="{{ $post->file }}" alt="Card image cap">
            @endif
            <div class="card-body">
                <h2 class="card-title">{{ $post->name }}</h2>
                <p class="card-text">{{ $post->excerpt }}</p>
                <a href="{{ route('post', $post->slug) }}" class="btn btn-primary">Read More &rarr;</a>
            </div>
            <div class="card-footer text-muted">
            {{-- Posted on January 1, 2017  --}}
            Posted on
            {{ Carbon\Carbon::parse($post->created_at)->format('l jS \\of F Y') }}
                by                
                <b>{{ $post->user->name }}</b>
            </div>
        </div>  
        
        @endforeach
                

        <!-- Pagination -->
        {{ $posts->render() }}

      </div>

      <!-- Sidebar Widgets Column -->
      {{-- @include('front.sections.sidebar') --}}

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  @include('front.sections.footer')
@endsection