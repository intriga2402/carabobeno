@extends('layouts.back')

@section('body-class', 'hold-transition sidebar-mini')

@section('content')
    <div class="wrapper">
        <!-- Navbar -->
        @include('backend.sections.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('backend.sections.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Blank Page</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Blank Page</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->

            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        Roles
                        @can('role-create')
                            <a href="{{ route('admin.roles.create') }}" class="btn btn-info float-right">Crear</a>                            
                        @endcan
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Nombre de Roles</th>
                                    <th colspan="2">&nbsp; Opciones</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($roles as $role)
                                    <tr>
                                        <td>{{ $role->name }}</td>
                                        <td width="10">
                                            @can('role-list')
                                                <a href="{{ route('admin.roles.show', $role->id) }}" class="btn btn-primary btn-sm">
                                                    <i class="fas fa-eye"></i>
                                                </a>
                                            @endcan
                                        </td>
                                        <td width="10">
                                            @can('role-edit')
                                                <a href="{{ route('admin.roles.edit', $role->id) }}" class="btn btn-warning btn-sm">
                                                    <i class="fas fa-edit"></i>
                                                </a>
                                            @endcan
                                        </td>
                                        <td width="10">
                                            @can('role-delete')
                                                {!! Form::open(['route' => ['admin.roles.destroy', $role->id], 'method' => 'DELETE']) !!}
                                                    <button class="btn btn-danger btn-sm"><i class="fas fa-eraser"></i></button>
                                                {!! Form::close() !!}
                                            @endcan
                                        </td>
                                    </tr>    
                                @endforeach                    

                            </tbody>
                        </table>

                        {{ $roles->render() }}

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>

        <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        @include('backend.sections.footer')

        <!-- /.control-sidebar -->
    </div>    
@endsection