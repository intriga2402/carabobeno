<div class="form-group">
    {{ Form::label('name', 'Nombre del rol') }}
    {{ Form::text('name', null, ['class' => 'form-control'] ) }}
</div>

<hr>


<h3>Lista de permisos</h3>
<div class="form-group">
    <ul class="list-unstyled">
        @foreach($permission as $value)
            <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}

                {{ $value->name }}</label>
            <br/>
        @endforeach
    </ul>
</div>

<div class="form-group">
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary btn-sm']) }}
</div>
