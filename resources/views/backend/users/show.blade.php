@extends('layouts.back')

@section('body-class', 'hold-transition sidebar-mini')

@section('content')
<div class="wrapper">
  <!-- Navbar -->
  @include('backend.sections.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('backend.sections.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blank Page</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->    
    <div class="col-12">
        <div class="card">

            <div class="card-header">
                Usuarios
                <a href="{{ route('admin.users') }}" class="btn btn-info float-right">Regresar</a>
            </div>
            <!-- /.card-header -->
            
            <div class="card-body">
                <h3 class="card-title">{{ $user->name }}</h3>
                
            </div>
        <!-- /.card-body -->
        </div>
    <!-- /.card -->
    </div>
    
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('backend.sections.footer')

  <!-- /.control-sidebar -->
</div>    
@endsection