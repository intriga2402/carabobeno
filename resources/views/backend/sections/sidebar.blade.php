<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        {{-- <img src="#" class="img-circle elevation-2" alt="User Image"> --}}
        </div>
        <div class="info">
        <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a href="{{ route('admin') }}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                            {{-- <i class="right fas fa-angle-left"></i> --}}
                        </p>
                    </a>
                </li>
                @can('post-list')
                    <li class="nav-item has-treeview">
                        <a href="{{ route('admin.posts') }}" class="nav-link">
                            <i class="nav-icon fas fa-book-reader"></i>
                            <p>
                                Publicaciones                
                            </p>
                        </a>
                    </li>
                @endcan

                @can('category-list')
                    <li class="nav-item has-treeview">
                        <a href="{{ route('admin.categories') }}" class="nav-link">
                            <i class="nav-icon fas fa-clipboard-list"></i>
                            <p>
                                Categorias             
                            </p>
                        </a>
                    </li>  
                @endcan
                
                @can('tag-list')
                    <li class="nav-item has-treeview">
                        <a href="{{ route('admin.tags') }}" class="nav-link">
                            <i class="nav-icon fas fa-copy"></i>
                            <p>
                                Etiquetas             
                            </p>
                        </a>
                    </li>
                @endcan

                @role('Admin')
                    <li class="nav-item has-treeview">
                        <a href="{{ route('admin.users') }}" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Usuarios             
                            </p>
                        </a>
                    </li>
                @endrole

                @role('Admin')
                    <li class="nav-item has-treeview">
                        <a href="{{ route('admin.roles') }}" class="nav-link">
                        <i class="nav-icon fas fa-address-card"></i>
                            <p>
                                Roles             
                            </p>
                        </a>
                    </li>  
                @endrole

                <li class="nav-item has-treeview">
                    <a href="{{ route('logout') }}" class="nav-link"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="nav-icon fas fa-share-square"></i>
                            <p>
                                {{ __('Logout') }}             
                            </p>
                        </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                    </form>
                </li>


            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>