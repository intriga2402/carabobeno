@extends('layouts.back')

@section('body-class', 'hold-transition sidebar-mini')

@section('content')
<div class="wrapper">
  <!-- Navbar -->
  @include('backend.sections.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('backend.sections.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blank Page</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    
    <div class="col-12">
        <div class="card">

            {{-- <div class="card-header">
                Categorias
                <a href="{{ route('admin.categories') }}" class="btn btn-info float-right">Regresar</a>
            </div> --}}
            <!-- /.card-header -->
            
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Crear Categorias</h3>
                    <a href="{{ route('admin.categories') }}" class="btn btn-info float-right">Regresar</a>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                {!! Form::open(['route' => 'admin.categories.store']) !!}
                {{ csrf_field() }}
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nombre de la categoria</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Categoria">
                            <input type="hidden" class="form-control" id="slug" name="slug">
                        </div>
                        <div class="form-group">
                            <label>Descripcion de la categoria</label>
                            <textarea class="form-control" rows="3" name="body" placeholder="Descripcion..."></textarea>
                        </div>
                    </div>
                <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right">Crear</button>
                    </div>
                {!! Form::close() !!}
            </div>
        <!-- /.card-body -->
        </div>
    <!-- /.card -->
    </div>
    
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('backend.sections.footer')

  <!-- /.control-sidebar -->
</div>    
@endsection

@push('scripts')  
    
    <script src="{{ secure_asset('js/jquery.stringToSlug.min.js') }}"></script>

    <script>
        $(document).ready( function() {
            $("#name, #slug").stringToSlug({
                callback: function(text){
                    $('#slug').val(text);
                }
            });
        });
    </script>
@endpush