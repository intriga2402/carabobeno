@extends('layouts.back')

@section('body-class', 'hold-transition sidebar-mini')

@section('content')
<div class="wrapper">
  <!-- Navbar -->
  @include('backend.sections.navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('backend.sections.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blank Page</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->    
    <div class="col-12">
        <div class="card">

            <div class="card-header">
                <h3 class="card-title">Publicaciones</h3>
                <a href="{{ route('admin.posts') }}" class="btn btn-info float-right">Regresar</a>
            </div>

            <div class="card mb-4">
                @if ($post->file)
                    <img class="card-img-top img-fluid" src="{{ $post->file }}" alt="Card image cap">
                @endif
                <div class="card-body">
                    
                    <h2 class="card-text">{!! $post->name !!}</h2>
                                    
                    <p class="card-text">{!! $post->excerpt !!}</p>
                    
                    <p class="card-text">{!! $post->body !!}</p>

                    <b>Categoria</b>
                    <p class="card-text">{!! $post->category->name !!}</p>

                    <b>Tags</b>
                    @foreach ($post->tags as $tag)
                        <p class="">
                          {{ $tag->name }}
                        </p>
                    @endforeach
                    
                </div>
                <div class="card-footer text-muted text-right">
                {{-- Posted on January 1, 2017  --}}
                Posted on
                {{ Carbon\Carbon::parse($post->created_at)->format('l jS \\of F Y') }}
                    by
                    <b>{!! $post->user->name !!}</b>
                </div>
            </div> 
            <!-- /.card-body -->
        </div>
    <!-- /.card -->
    </div>
    
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  @include('backend.sections.footer')

  <!-- /.control-sidebar -->
</div>    
@endsection