{{ Form::hidden('user_id', auth()->user()->id) }}   

<div class="row">
    <div class="col-sm">
        <div class="form-group">
            {{ Form::label('category_id', 'Categorias') }}
            {{ Form::select('category_id', $categories, null, ['class' => 'form-control select2bs4']) }}
        </div>
    </div>     
</div>

<div class="form-group">
    {{ Form::label('name', 'Nombre de la publicacion') }}
    {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
</div>

<div class="form-group">
    {{-- {{ Form::label('slug', 'Url Amigable') }} --}}
    {{ Form::hidden('slug', null, ['class' => 'form-control', 'id' => 'slug']) }}
</div>

<div class="form-group">
    {{ Form::label('file', 'Imagen') }}
    {{ Form::file('file') }}
</div>
{{-- <div class="form-group">
    <label for="exampleInputFile">Subir imagen</label>
    <div class="input-group">
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="exampleInputFile">
            <label class="custom-file-label" for="exampleInputFile">Elegir archivo</label>
        </div>
    </div>
</div> --}}

<div class="form-group">
    {{ Form::label('status', 'Estado') }}
<label>
    {{ Form::radio('status', 'PUBLISHED') }} Publicado
</label>
<label>
    {{ Form::radio('status', 'DRAFT') }} Borrador
</label>
</div>


<div class="form-group">
    {{ Form::label('tags', 'Etiquetas') }}
    <div>
        @foreach ($tags as $tag)
        <label>
            {{ Form::checkbox('tags[]', $tag->id) }} {{ $tag->name }}
        </label>
        @endforeach
    </div>
</div>

<div class="form-group">
    {{ Form::label('excerpt', 'Descripcion') }}
    {{ Form::textarea('excerpt', null, ['class' => 'form-control', 'rows' => '2']) }}
</div>

<div class="form-group">
    {{ Form::label('body', 'Contenido') }}
    {{ Form::textarea('body', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::submit('Guardar', ['class' => 'btn btn-primary float-right']) }}
</div>

@push('scripts')  
    
    <script src="{{ secure_asset('js/jquery.stringToSlug.min.js') }}"></script>
    <script src="{{ secure_asset('css/backend/select2/js/select2.min.js') }}"></script>
    <script src="{{ secure_asset('css/backend/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <script src="{{ secure_asset('js/ckeditor/ckeditor.js') }}"></script>

    <script>
        $(document).ready( function() {
            // string to slug
            $("#name, #slug").stringToSlug({
                callback: function(text){
                    $('#slug').val(text);
                }
            });

            // select2
            $('.select2').select2()
    
            //Initialize Select2 Elements
            $('.select2bs4').select2({
            theme: 'bootstrap4'
            })

            // bs-custom-file-input
            $(document).ready(function () {
                bsCustomFileInput.init();
            });

            // CKEDITOR
            CKEDITOR.config.height = 400;
            CKEDITOR.config.width = 'auto';
            CKEDITOR.replace('body');
        });
    </script>
@endpush