<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Category;
use App\Post;
use App\User;
use App\Tag;


class FrontController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->where('status', 'PUBLISHED')->paginate(3);
        //dd($posts);
        return view('front.posts', compact('posts'));
    }


    public function post($slug)
    {
        $post = Post::where('slug', $slug)->first();
        return view('front.post', compact('post'));
    }

        public function category($slug)
    {
        $category = Category::where('slug', $slug)
                  ->pluck('id')
                  ->first();
        $posts = Post::where('category_id', $category)
               ->orderBy('id', 'desc')->where('status', 'PUBLISHED')->paginate(3);
        //dd($posts);
        return view('front.posts', compact('posts'));
    }
    
    public function tag($slug)
    {
        $posts = Post::whereHas('tags', function($query) use($slug){
            $query->where('slug', $slug);    
        })
        ->orderBy('id', 'desc')->where('status', 'PUBLISHED')->paginate(3);
        //dd($posts);
        return view('front.posts', compact('posts'));
    }    

        
}
