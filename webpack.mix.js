const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');


/* FRONT END */
mix.styles([
   'public/css/bootstrap/bootstrap.min.css',
   'public/css/styles/blog-home.css'
], 'public/css/front.css');


mix.scripts([
   'public/js/bootstrap/jquery.min.js',
   'public/js/bootstrap/bootstrap.bundle.min.js'
], 'public/js/front.js');



